package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.IllagerRegistry;
import com.legacy.conjurer_illager.client.particle.PlayingCardParticle;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleType;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.event.RegistryEvent.Register;

public class IllagerParticles
{
	public static final BasicParticleType RED_PLAYING_CARD = new BasicParticleType(false);
	public static final BasicParticleType BLACK_PLAYING_CARD = new BasicParticleType(false);

	public static void init(Register<ParticleType<?>> event)
	{
		register(event, "red_playing_card", RED_PLAYING_CARD);
		register(event, "black_playing_card", BLACK_PLAYING_CARD);
	}

	private static void register(Register<ParticleType<?>> event, String key, ParticleType<?> particle)
	{
		IllagerRegistry.register(event.getRegistry(), key, particle);
	}

	public static void registerFactories(ParticleFactoryRegisterEvent event)
	{
		registerFactory(RED_PLAYING_CARD, PlayingCardParticle.Factory::new);
		registerFactory(BLACK_PLAYING_CARD, PlayingCardParticle.Factory::new);
	}

	private static <T extends IParticleData> void registerFactory(ParticleType<T> particleTypeIn, ParticleManager.IParticleMetaFactory<T> particleMetaFactoryIn)
	{
		Minecraft mc = Minecraft.getInstance();
		mc.particles.registerFactory(particleTypeIn, particleMetaFactoryIn);
	}
}
