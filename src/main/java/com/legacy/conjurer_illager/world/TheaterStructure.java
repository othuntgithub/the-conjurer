package com.legacy.conjurer_illager.world;

import java.util.Arrays;
import java.util.Random;

import com.google.common.collect.Lists;
import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.IllagerRegistry;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerStructures;
import com.legacy.structure_gel.util.ConfigTemplates.StructureConfig;
import com.legacy.structure_gel.worldgen.jigsaw.AbstractGelStructurePiece;
import com.legacy.structure_gel.worldgen.jigsaw.GelConfigJigsawStructure;
import com.mojang.serialization.Codec;

import net.minecraft.block.Blocks;
import net.minecraft.block.ChestBlock;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.item.HangingEntity;
import net.minecraft.entity.item.PaintingEntity;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.monster.PillagerEntity;
import net.minecraft.entity.passive.fish.TropicalFishEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.tileentity.LecternTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.gen.feature.jigsaw.JigsawManager.IPieceFactory;
import net.minecraft.world.gen.feature.jigsaw.JigsawPiece;
import net.minecraft.world.gen.feature.structure.IStructurePieceType;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class TheaterStructure extends GelConfigJigsawStructure
{
	public TheaterStructure(Codec<VillageConfig> codec, StructureConfig config)
	{
		super(codec, config, 0, true, true);
		this.setSpawnList(EntityClassification.MONSTER, Lists.newArrayList());
	}

	@Override
	public int getSeed()
	{
		return 257857;
	}

	@Override
	public IPieceFactory getPieceType()
	{
		return TheaterStructure.Piece::new;
	}

	public static class Piece extends AbstractGelStructurePiece
	{
		public Piece(TemplateManager template, JigsawPiece jigsawPiece, BlockPos pos, int groundLevelDelta, Rotation rotation, MutableBoundingBox boundingBox)
		{
			super(template, jigsawPiece, pos, groundLevelDelta, rotation, boundingBox);
		}

		public Piece(TemplateManager template, CompoundNBT nbt)
		{
			super(template, nbt);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, IServerWorld worldIn, Random rand, MutableBoundingBox bounds)
		{
			if (key.equals("conjurer"))
			{
				setAir(worldIn, pos);
				ConjurerEntity entity = createEntity(IllagerEntityTypes.CONJURER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("admissioner"))
			{
				setAir(worldIn, pos);
				PillagerEntity entity = createEntity(EntityType.PILLAGER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("villager"))
			{
				setAir(worldIn, pos);
				VillagerEntity entity = createEntity(EntityType.VILLAGER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				worldIn.addEntity(entity);

				entity.goalSelector.addGoal(9, new LookAtGoal(entity, ConjurerEntity.class, 15.0F, 2.0F));
			}

			if (key.equals("audience"))
			{
				setAir(worldIn, pos);
				MobEntity entity = createEntity(EntityType.PILLAGER, worldIn, pos, this.rotation);

				if (rand.nextFloat() < 0.05F)
				{
					entity = createEntity(EntityType.ILLUSIONER, worldIn, pos, this.rotation);
				}
				else if (rand.nextFloat() < 0.40F)
				{
					entity = createEntity(EntityType.VINDICATOR, worldIn, pos, this.rotation);
				}
				else if (rand.nextFloat() < 0.20F)
				{
					entity = createEntity(EntityType.WITCH, worldIn, pos, this.rotation);
				}

				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);

				entity.goalSelector.addGoal(9, new LookAtGoal(entity, ConjurerEntity.class, 15.0F, 2.0F));
			}

			if (key.contains("lectern"))
			{
				setAir(worldIn, pos);

				ItemStack bookStack = new ItemStack(Items.WRITTEN_BOOK);
				CompoundNBT bookNBT = bookStack.getOrCreateTag();
				ListNBT bookPages = new ListNBT();

				String pageText = "Free Admission List\n -=-=-=-=-=-=-=-=-\n";
				String[] names = ConjurerIllagerMod.SUPPORTERS.getSupporters().stream().map(mls -> mls.name).toArray(String[]::new);

				if (names.length > 0)
				{
					for (int i = 0; i < 8; i++)
					{
						String name = names[rand.nextInt(names.length)];
						pageText = pageText + name + "\n";
						names = Arrays.asList(names).stream().filter(s -> s != name).toArray(String[]::new);
					}
					bookPages.add(StringNBT.valueOf(ITextComponent.Serializer.toJson(new StringTextComponent(pageText))));

					bookNBT.putString("title", "Admission List");
					bookNBT.putString("author", "The Conjurer");
					bookNBT.put("pages", bookPages);

					bookStack.setTag(bookNBT);
					LecternTileEntity lectern = (LecternTileEntity) worldIn.getTileEntity(pos.down());
					lectern.setBook(bookStack);
				}
			}

			if (key.contains("chest"))
			{
				String[] data = key.split("-");
				Direction facing = Direction.byName(data[1]);

				worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
				worldIn.setBlockState(pos, Blocks.CHEST.getDefaultState().with(ChestBlock.FACING, facing).rotate(worldIn, pos, this.rotation), 3);
			}

			if (key.equals("fish"))
			{
				worldIn.setBlockState(pos, Blocks.SANDSTONE.getDefaultState(), 3);
				TropicalFishEntity entity = createEntity(EntityType.TROPICAL_FISH, worldIn, pos.up(rand.nextInt(3) + 1), this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.contains("art"))
			{
				String[] data = key.split("-");
				Direction facing = Direction.byName(data[1]);

				setAir(worldIn, pos);
				PaintingEntity entity = createEntity(EntityType.PAINTING, worldIn, pos, this.rotation);
				entity.art = IllagerRegistry.THEATER_PAINTING;

				try
				{
					ObfuscationReflectionHelper.setPrivateValue(HangingEntity.class, entity, this.rotation.rotate(facing), "field_174860_b"); // facingDirection
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				worldIn.addEntity(entity);
			}
		}

		@Override
		public IStructurePieceType getStructurePieceType()
		{
			return IllagerStructures.THEATER.getPieceType();
		}
	}
}
