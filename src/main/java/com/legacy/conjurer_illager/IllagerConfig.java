package com.legacy.conjurer_illager;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.structure_gel.util.ConfigTemplates;

import net.minecraftforge.common.ForgeConfigSpec;

public class IllagerConfig
{
	public static final Common COMMON;
	public static final ForgeConfigSpec COMMON_SPEC;
	static
	{
		Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPair.getRight();
		COMMON = specPair.getLeft();
	}

	public static class Common
	{
		public final ConfigTemplates.StructureConfig theaterStructureConfig;

		public Common(ForgeConfigSpec.Builder builder)
		{
			this.theaterStructureConfig = new ConfigTemplates.StructureConfig(builder, "theater", 0.50D, 40, 10).biomes(true, "dark_forest, dark_forest_hills");
		}
	}
}
