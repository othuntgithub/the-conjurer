package com.legacy.conjurer_illager.client.model;

import com.google.common.collect.ImmutableList;
import com.legacy.conjurer_illager.entity.ThrowingCardEntity;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ThrowingCardModel extends SegmentedModel<ThrowingCardEntity>
{
	private final ModelRenderer card;

	public ThrowingCardModel()
	{
		textureWidth = 32;
		textureHeight = 32;

		card = new ModelRenderer(this);
		card.setRotationPoint(0.0F, 24.0F, 0.0F);
		card.setTextureOffset(0, 0).addBox(-2.0F, -1.0F, -3.0F, 4.0F, 1.0F, 6.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(ThrowingCardEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.card);
	}
}