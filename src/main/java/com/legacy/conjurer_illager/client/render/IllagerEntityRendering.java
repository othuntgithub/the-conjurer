package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class IllagerEntityRendering
{

	public static void init()
	{
		register(IllagerEntityTypes.CONJURER, ConjurerRenderer::new);
		register(IllagerEntityTypes.BOUNCING_BALL, BouncingBallRenderer::new);
		register(IllagerEntityTypes.THROWING_CARD, ThrowingCardRenderer::new);
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}