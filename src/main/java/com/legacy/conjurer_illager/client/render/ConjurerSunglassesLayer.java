package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.IllagerModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ConjurerSunglassesLayer<T extends ConjurerEntity, M extends IllagerModel<T> & IHasArm> extends LayerRenderer<T, M>
{
	private static final ResourceLocation TEXTURE = ConjurerIllagerMod.locate("textures/entity/sunglasses.png");

	private final IllagerModel<ConjurerEntity> illagerModel = new IllagerModel<ConjurerEntity>(0.3F, 0.0F, 128, 64);

	public ConjurerSunglassesLayer(IEntityRenderer<T, M> renderer)
	{
		super(renderer);
	}

	@SuppressWarnings("unchecked")
	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (!entitylivingbaseIn.isInvisible() && entitylivingbaseIn.hasCustomName() && ("Vamacheron".equals(entitylivingbaseIn.getName().getUnformattedComponentText()) || "Vamacher0n".equals(entitylivingbaseIn.getName().getUnformattedComponentText())))
		{
			this.getEntityModel().copyModelAttributesTo((IllagerModel<T>) this.illagerModel);
			this.illagerModel.setLivingAnimations(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks);
			this.illagerModel.setRotationAngles(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
			IVertexBuilder ivertexbuilder = bufferIn.getBuffer(RenderType.getEntityTranslucentCull(TEXTURE));
			this.illagerModel.render(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}