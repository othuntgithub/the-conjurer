package com.legacy.conjurer_illager.entity;

import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerSounds;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class BouncingBallEntity extends ThrowableEntity
{
	private static final DataParameter<Integer> BALL_TYPE = EntityDataManager.<Integer>createKey(BouncingBallEntity.class, DataSerializers.VARINT);
	private static final DataParameter<Integer> BOUNCES = EntityDataManager.<Integer>createKey(BouncingBallEntity.class, DataSerializers.VARINT);

	public BouncingBallEntity(EntityType<? extends BouncingBallEntity> type, World world)
	{
		super(type, world);
	}

	public BouncingBallEntity(EntityType<? extends BouncingBallEntity> type, LivingEntity thrower, World world)
	{
		super(type, thrower, world);
	}

	public BouncingBallEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(IllagerEntityTypes.BOUNCING_BALL, world);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("ballType", this.getBallType());
		compound.putInt("totalBounces", this.getTotalBounces());
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
		this.setBallType(compound.getInt("ballType"));
		this.setTotalBounces(compound.getInt("totalBounces"));
	}

	@Override
	protected void onImpact(RayTraceResult result)
	{
		RayTraceResult.Type raytraceresult$type = result.getType();
		if (raytraceresult$type == RayTraceResult.Type.BLOCK)
		{
			BlockRayTraceResult traceResult = (BlockRayTraceResult) result;
			BlockState blockstate = this.world.getBlockState(traceResult.getPos());
			if (!blockstate.getCollisionShape(this.world, traceResult.getPos()).isEmpty())
			{
				Direction face = traceResult.getFace();
				blockstate.onProjectileCollision(this.world, blockstate, traceResult, this);

				Vector3d motion = this.getMotion();

				double motionX = motion.getX();
				double motionY = motion.getY();
				double motionZ = motion.getZ();

				if (face == Direction.EAST)
					motionX = -motionX;
				else if (face == Direction.SOUTH)
					motionZ = -motionZ;
				else if (face == Direction.WEST)
					motionX = -motionX;
				else if (face == Direction.NORTH)
					motionZ = -motionZ;
				else if (face == Direction.UP)
					motionY = -motionY;
				else if (face == Direction.DOWN)
					motionY = -motionY;

				this.setMotion(motionX, motionY, motionZ);

				if (this.ticksExisted > 200 || this.getTotalBounces() > 20)
				{
					this.playSound(IllagerSounds.ENTITY_BOUNCY_BALL_VANISH, 1.0F, 2.0F);
					this.remove();
				}
				else
				{
					this.playSound(IllagerSounds.ENTITY_BOUNCY_BALL_BOUNCE, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
					this.setTotalBounces(this.getTotalBounces() + 1);
				}
			}
		}

		if (raytraceresult$type == RayTraceResult.Type.ENTITY)
		{
			Entity entity = ((EntityRayTraceResult) result).getEntity();

			// func_234616_v_ = getThrower
			if (this.func_234616_v_() != null && !entity.isOnSameTeam(this.func_234616_v_()) && !(entity instanceof RabbitEntity))
			{
				int i = 5 + this.world.getDifficulty().getId();

				if (!entity.isInvulnerable())
				{
					entity.attackEntityFrom(DamageSource.causeThrownDamage(this, this.func_234616_v_()).setDifficultyScaled(), (float) i);
					this.playSound(SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, 0.5F, 2.0F);
					this.playSound(IllagerSounds.ENTITY_BOUNCY_BALL_VANISH, 1.0F, 2.0F);
					this.remove();
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.hasNoGravity())
			this.setMotion(this.getMotion().scale(1.01D));

		if (this.ticksExisted > 360 || this.ticksExisted > 150 && this.world.canSeeSky(this.getPosition()) || this.func_234616_v_() != null && !this.func_234616_v_().isAlive())
		{
			this.playSound(IllagerSounds.ENTITY_BOUNCY_BALL_VANISH, 1.0F, 2.0F);
			this.remove();
		}

		for (int i = 0; i < 2; ++i)
		{
			this.world.addParticle(ParticleTypes.WITCH, this.getPosXRandom(0.5D), this.getPosYRandom(), this.getPosZRandom(0.5D), this.rand.nextGaussian() * 0.1D, 0.0D, this.rand.nextGaussian() * 0.1D);
		}
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	protected void registerData()
	{
		this.dataManager.register(BALL_TYPE, Integer.valueOf(0));
		this.dataManager.register(BOUNCES, Integer.valueOf(0));
	}

	public int getBallType()
	{
		return this.dataManager.get(BALL_TYPE);
	}

	public void setBallType(int type)
	{
		this.dataManager.set(BALL_TYPE, Integer.valueOf(type));
	}

	public int getTotalBounces()
	{
		return this.dataManager.get(BOUNCES);
	}

	public void setTotalBounces(int bounces)
	{
		this.dataManager.set(BOUNCES, Integer.valueOf(bounces));
	}
}
