package com.legacy.conjurer_illager.client;

import com.legacy.conjurer_illager.client.render.IllagerEntityRendering;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class IllagerClient
{
	public static void initialization(FMLClientSetupEvent event)
	{
		IllagerEntityRendering.init();
		IllagerColoring.ItemCol.init();
	}
}