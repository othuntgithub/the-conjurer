package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix3f;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BouncingBallRenderer extends EntityRenderer<BouncingBallEntity>
{
	private static final ResourceLocation[] BALLS = new ResourceLocation[] { ConjurerIllagerMod.locate("textures/entity/bouncy_ball/stripe_1.png"), ConjurerIllagerMod.locate("textures/entity/bouncy_ball/stripe_2.png"), ConjurerIllagerMod.locate("textures/entity/bouncy_ball/stripes_1.png"), ConjurerIllagerMod.locate("textures/entity/bouncy_ball/stripes_2.png"), ConjurerIllagerMod.locate("textures/entity/bouncy_ball/spots_1.png"), ConjurerIllagerMod.locate("textures/entity/bouncy_ball/spots_2.png") };
	private static final RenderType[] RENDER_TYPES = new RenderType[] { RenderType.getEntityCutoutNoCull(BALLS[0]), RenderType.getEntityCutoutNoCull(BALLS[1]), RenderType.getEntityCutoutNoCull(BALLS[2]), RenderType.getEntityCutoutNoCull(BALLS[3]), RenderType.getEntityCutoutNoCull(BALLS[4]), RenderType.getEntityCutoutNoCull(BALLS[5]) };

	public BouncingBallRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public void render(BouncingBallEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		matrixStackIn.push();
		matrixStackIn.scale(0.8F, 0.8F, 0.8F);
		matrixStackIn.rotate(this.renderManager.getCameraOrientation());
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180.0F));
		MatrixStack.Entry matrixstack$entry = matrixStackIn.getLast();
		Matrix4f matrix4f = matrixstack$entry.getMatrix();
		Matrix3f matrix3f = matrixstack$entry.getNormal();
		IVertexBuilder ivertexbuilder = bufferIn.getBuffer(RENDER_TYPES[entityIn.getBallType()]);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 0.0F, 0, 0, 1);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 1.0F, 0, 1, 1);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 1.0F, 1, 1, 0);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 0.0F, 1, 0, 0);
		matrixStackIn.pop();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	private static void setupVertex(IVertexBuilder builder, Matrix4f matrix4f, Matrix3f matrix3f, int p_229045_3_, float p_229045_4_, int p_229045_5_, int p_229045_6_, int p_229045_7_)
	{
		builder.pos(matrix4f, p_229045_4_ - 0.5F, (float) p_229045_5_ - 0.25F, 0.0F).color(255, 255, 255, 255).tex((float) p_229045_6_, (float) p_229045_7_).overlay(OverlayTexture.NO_OVERLAY).lightmap(p_229045_3_).normal(matrix3f, 0.0F, 1.0F, 0.0F).endVertex();
	}

	@Override
	public ResourceLocation getEntityTexture(BouncingBallEntity entity)
	{
		return BALLS[entity.getBallType()];
	}
}