package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.client.model.ThrowingCardModel;
import com.legacy.conjurer_illager.entity.ThrowingCardEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ThrowingCardRenderer extends EntityRenderer<ThrowingCardEntity>
{
	private static final ResourceLocation[] CARDS = new ResourceLocation[] { ConjurerIllagerMod.locate("textures/entity/throwing_card/red_throwing_card.png"), ConjurerIllagerMod.locate("textures/entity/throwing_card/black_throwing_card.png"), ConjurerIllagerMod.locate("textures/entity/throwing_card/blue_throwing_card.png") };
	private static final ThrowingCardModel model = new ThrowingCardModel();

	public ThrowingCardRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public void render(ThrowingCardEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		matrixStackIn.push();
		matrixStackIn.translate(0.0D, (double) -0.23F, 0.0D);
		matrixStackIn.scale(0.8F, 0.2F, 0.8F);
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(MathHelper.lerp(partialTicks, entityIn.prevRotationYaw, entityIn.rotationYaw)));
		model.setRotationAngles(entityIn, partialTicks, 0.0F, -0.1F, 0.0F, 0.0F);
		IVertexBuilder ivertexbuilder = bufferIn.getBuffer(model.getRenderType(CARDS[entityIn.getCardType()]));
		model.render(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		matrixStackIn.pop();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	public ResourceLocation getEntityTexture(ThrowingCardEntity entity)
	{
		return CARDS[entity.getCardType()];
	}
}