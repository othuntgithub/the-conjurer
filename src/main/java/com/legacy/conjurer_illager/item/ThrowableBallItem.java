package com.legacy.conjurer_illager.item;

import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

public class ThrowableBallItem extends Item
{
	public ThrowableBallItem(Item.Properties builder)
	{
		super(builder);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		worldIn.playSound((PlayerEntity) null, playerIn.getPosX(), playerIn.getPosY(), playerIn.getPosZ(), SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));

		if (!worldIn.isRemote)
		{
			BouncingBallEntity snowballentity = new BouncingBallEntity(IllagerEntityTypes.BOUNCING_BALL, playerIn, worldIn);
			snowballentity.func_234612_a_(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 1.0F, 1F, 3F); // shoot
			snowballentity.setBallType(worldIn.rand.nextInt(5));
			worldIn.addEntity(snowballentity);
		}

		playerIn.addStat(Stats.ITEM_USED.get(this));
		if (!playerIn.abilities.isCreativeMode)
		{
			itemstack.shrink(1);
		}

		return ActionResult.resultSuccess(itemstack);
	}
}