package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.IllagerConfig;
import com.legacy.conjurer_illager.world.TheaterPools;
import com.legacy.conjurer_illager.world.TheaterStructure;
import com.legacy.structure_gel.access_helpers.JigsawAccessHelper;
import com.legacy.structure_gel.registrars.StructureRegistrar;
import com.legacy.structure_gel.util.RegistryHelper;

import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class IllagerStructures
{
	public static final StructureRegistrar<VillageConfig, TheaterStructure> THEATER = RegistryHelper.handleRegistrar(StructureRegistrar.of(ConjurerIllagerMod.locate("theatre"), new TheaterStructure(VillageConfig.field_236533_a_, IllagerConfig.COMMON.theaterStructureConfig), TheaterStructure.Piece::new, new VillageConfig(() -> TheaterPools.ROOT, 7), Decoration.SURFACE_STRUCTURES));

	@SubscribeEvent
	public static void init(RegistryEvent.Register<Structure<?>> event)
	{
		TheaterPools.init();
		RegistryHelper.handleRegistrar(THEATER, event.getRegistry());
		JigsawAccessHelper.addIllagerStructures(THEATER.getStructure());
	}
}