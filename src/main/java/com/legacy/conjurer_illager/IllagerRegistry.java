package com.legacy.conjurer_illager;

import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.item.IllagerArmorMaterial;
import com.legacy.conjurer_illager.item.MagicianHatItem;
import com.legacy.conjurer_illager.item.ThrowableBallItem;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerParticles;
import com.legacy.conjurer_illager.registry.IllagerSounds;
import com.legacy.conjurer_illager.registry.IllagerStructures;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.entity.item.PaintingType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.particles.ParticleType;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = ConjurerIllagerMod.MODID, bus = Bus.MOD)
public class IllagerRegistry
{
	public static final Item CONJURER_HAT = new MagicianHatItem(IllagerArmorMaterial.CONJURER_HAT, EquipmentSlotType.HEAD, new Item.Properties().group(ItemGroup.COMBAT));
	public static final Item CONJURER_SPAWN_EGG = new SpawnEggItem(IllagerEntityTypes.CONJURER, 0x71265b, 0xe79e4a, new Item.Properties().group(ItemGroup.MISC));
	public static final Item THROWABLE_BALL = new ThrowableBallItem(new Item.Properties().group(null));

	public static final PaintingType THEATER_PAINTING = new PaintingType(32, 32);

	@SubscribeEvent
	public static void registerParticles(Register<ParticleType<?>> event)
	{
		IllagerParticles.init(event);
	}

	@SubscribeEvent
	public static void registerParticleFactories(ParticleFactoryRegisterEvent event)
	{
		IllagerParticles.registerFactories(event);
	}

	@SubscribeEvent
	public static void onRegisterPaintings(Register<PaintingType> event)
	{
		register(event.getRegistry(), "theater", THEATER_PAINTING);
	}

	@SubscribeEvent
	public static void onRegisterSounds(Register<SoundEvent> event)
	{
		IllagerSounds.init(event);
	}

	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		register(event.getRegistry(), "conjurer_hat", CONJURER_HAT);
		register(event.getRegistry(), "conjurer_spawn_egg", CONJURER_SPAWN_EGG);
		register(event.getRegistry(), "throwable_ball", THROWABLE_BALL);
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		IllagerEntityTypes.init(event);

		GlobalEntityTypeAttributes.put(IllagerEntityTypes.CONJURER, ConjurerEntity.registerAttributeMap().create());
	}

	@SubscribeEvent
	public static void onRegisterStructures(Register<Structure<?>> event)
	{
		IllagerStructures.init(event);
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(ConjurerIllagerMod.locate(name));
		registry.register(object);
	}
}