package com.legacy.conjurer_illager.world;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.registry.IllagerProcessors;
import com.legacy.structure_gel.worldgen.jigsaw.JigsawRegistryHelper;

import net.minecraft.world.gen.feature.jigsaw.JigsawPattern;

public class TheaterPools
{
	public static final JigsawPattern ROOT;

	public static void init()
	{
	}

	static
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(ConjurerIllagerMod.MODID, "theatre/");

		ROOT = registry.register("root", registry.builder().names("root").build());
		registry.register("lobby", registry.builder().names("lobby_1", "lobby_2").maintainWater(false).build());
		registry.register("stage", registry.builder().names("stage_1", "stage_2").maintainWater(false).processors(IllagerProcessors.COBBLE_WALLS_TO_GRASS).build());
		registry.register("offshoot", registry.builder().names("offshoot/closet", "offshoot/dressing_room").maintainWater(false).processors(IllagerProcessors.COBBLE_WALLS_TO_GRASS).build());
		registry.register("scene", registry.builder().names("scene/joker", "scene/titanic", "scene/superman", "scene/indiana_jones", "scene/wizard_of_oz").maintainWater(false).build());
	}
}
