package com.legacy.conjurer_illager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.conjurer_illager.client.IllagerClient;
import com.legacy.conjurer_illager.registry.IllagerStructures;
import com.legacy.structure_gel.access_helpers.BiomeAccessHelper;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(ConjurerIllagerMod.MODID)
public class ConjurerIllagerMod
{
	public static final Logger LOGGER = LogManager.getLogger();
	public static final String NAME = "The Conjurer";
	public static final String MODID = "conjurer_illager";
	public static final MLSupporter.Supporters SUPPORTERS = new MLSupporter.Supporters();

	public ConjurerIllagerMod()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, IllagerConfig.COMMON_SPEC);

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus().addListener(IllagerClient::initialization));
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonInit);
	}

	private void commonInit(FMLCommonSetupEvent event)
	{
		new MLSupporter.GetSupportersThread().start();

		MinecraftForge.EVENT_BUS.register(new IllagerEvents());
		MinecraftForge.EVENT_BUS.addListener((BiomeLoadingEvent biomeEvent) -> BiomeAccessHelper.addStructureIfAllowed(biomeEvent, IllagerStructures.THEATER.getStructureFeature()));
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return new String(MODID + ":" + name);
	}

	public static class MLSupporter
	{
		public final String name;
		public final UUID uuid;

		public MLSupporter(String name, String uuid)
		{
			this.name = name;
			this.uuid = UUID.fromString(uuid);
		}

		@Override
		public String toString()
		{
			return String.format("name=%s, UUID=%s", this.name, this.uuid.toString());
		}

		public static class Supporters
		{
			private List<MLSupporter> supporters = new ArrayList<>();

			public boolean isSupporter(PlayerEntity player)
			{
				return this.supporters.stream().filter(s -> player.getUniqueID().equals(s.uuid)).findAny().isPresent();
			}

			public List<MLSupporter> getSupporters()
			{
				return this.supporters;
			}

			@Override
			public String toString()
			{
				String output = "[";
				for (int i = 0; i < this.supporters.size(); i++)
					output.concat(this.supporters.get(i).toString() + (i < this.supporters.size() - 1 ? ", " : ""));
				output.concat("]");
				return output;
			}
		}

		protected static class GetSupportersThread extends Thread
		{
			public GetSupportersThread()
			{
				super("The Conjurer supporters thread");
				this.setDaemon(true);
			}

			@Override
			public void run()
			{
				try
				{
					List<String> names = new ArrayList<>();
					URL url = new URL("https://moddinglegacy.com/supporters-changelogs/supporters.txt");
					HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
					httpcon.addRequestProperty("User-Agent", "Mozilla/4.0");

					BufferedReader reader = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));

					String line;
					while ((line = reader.readLine()) != null)
						names.add(line);
					reader.close();

					loadSupporters(names.stream().filter(s -> !(s.isEmpty() || s.startsWith("#"))).map(s ->
					{
						String[] values = s.split(",");
						return new MLSupporter(values[0], values[1]);
					}).collect(Collectors.toList()));
				}
				catch (IOException e)
				{
					ConjurerIllagerMod.LOGGER.info("Couldn't load the Modding Legacy supporters list. You may be offline or our website could be having issues. If you are a supporter, some cosmetic features may not work.");
					e.printStackTrace();
				}
				catch (Exception e)
				{
					ConjurerIllagerMod.LOGGER.info("Failed to load the Modding Legacy supporters list. If you are a supporter, some cosmetic features may not work.");
					e.printStackTrace();
				}
			}

			private void loadSupporters(List<MLSupporter> supporters)
			{
				ConjurerIllagerMod.SUPPORTERS.getSupporters().addAll(supporters);
			}
		}
	}
}
