package com.legacy.conjurer_illager.registry;

import com.google.common.collect.ImmutableList;
import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.structure_gel.util.RegistryHelper;

import net.minecraft.block.Blocks;
import net.minecraft.world.gen.feature.template.BlockMatchRuleTest;
import net.minecraft.world.gen.feature.template.RuleEntry;
import net.minecraft.world.gen.feature.template.RuleStructureProcessor;
import net.minecraft.world.gen.feature.template.StructureProcessor;
import net.minecraft.world.gen.feature.template.StructureProcessorList;

public class IllagerProcessors
{
	public static final StructureProcessorList COBBLE_WALLS_TO_GRASS = register("cobble_walls_to_grass", new RuleStructureProcessor(ImmutableList.of(new RuleEntry(new BlockMatchRuleTest(Blocks.COBBLESTONE_WALL), new BlockMatchRuleTest(Blocks.GRASS_BLOCK), Blocks.GRASS_BLOCK.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.COBBLESTONE_WALL), new BlockMatchRuleTest(Blocks.DIRT), Blocks.DIRT.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.COBBLESTONE_WALL), new BlockMatchRuleTest(Blocks.STONE), Blocks.STONE.getDefaultState()))));

	private static StructureProcessorList register(String key, StructureProcessor processor)
	{
		return RegistryHelper.registerProcessor(ConjurerIllagerMod.locate(key), processor);
	}
}
