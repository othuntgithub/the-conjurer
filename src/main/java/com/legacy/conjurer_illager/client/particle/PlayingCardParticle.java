package com.legacy.conjurer_illager.client.particle;

import net.minecraft.client.particle.IAnimatedSprite;
import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.SpriteTexturedParticle;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class PlayingCardParticle extends SpriteTexturedParticle
{
	private final IAnimatedSprite spriteSet;

	private PlayingCardParticle(ClientWorld worldIn, double x, double y, double z, double dx, double dy, double dz, IAnimatedSprite spriteSet)
	{
		super(worldIn, x, y, z, dx, dy, dz);
		this.spriteSet = spriteSet;
		this.motionX = this.motionX * (double) 0.01F + dx;
		this.motionY = this.motionY * (double) 0.01F + dy;
		this.motionZ = this.motionZ * (double) 0.01F + dz;
		this.posX += (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.05F);
		this.posY += (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.05F);
		this.posZ += (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.05F);
		this.maxAge = (int) (8.0D / (Math.random() * 0.3D + 0.2D)) + 14;
		this.selectSpriteWithAge(spriteSet);
	}

	@Override
	public IParticleRenderType getRenderType()
	{
		return IParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	@Override
	public void move(double x, double y, double z)
	{
		this.setBoundingBox(this.getBoundingBox().offset(x, y, z));
		this.resetPositionToBB();
	}

	@Override
	public float getScale(float partialTicks)
	{
		return 0.12F;
	}

	@Override
	public int getBrightnessForRender(float partialTick)
	{
		return super.getBrightnessForRender(partialTick);
	}

	@Override
	public void tick()
	{
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		if (this.age++ >= this.maxAge)
		{
			this.setExpired();
		}
		else
		{
			this.selectSpriteWithAge(spriteSet);
			this.move(this.motionX, this.motionY, this.motionZ);

			this.motionX *= (double) 0.96F;
			this.motionY *= (double) 0.96F;
			this.motionZ *= (double) 0.96F;
			if (this.onGround)
			{
				this.motionX *= (double) 0.7F;
				this.motionZ *= (double) 0.7F;
			}

			if (this.age < this.maxAge / 2)
			{
				this.motionY = this.motionY - 0.01F;
			}
		}
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements IParticleFactory<BasicParticleType>
	{
		private final IAnimatedSprite spriteSet;

		public Factory(IAnimatedSprite spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		@Override
		public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			PlayingCardParticle particle = new PlayingCardParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, spriteSet);
			return particle;
		}
	}
}