package com.legacy.conjurer_illager.entity;

import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class ThrowingCardEntity extends ThrowableEntity
{
	private static final DataParameter<Integer> CARD_TYPE = EntityDataManager.<Integer>createKey(ThrowingCardEntity.class, DataSerializers.VARINT);

	public ThrowingCardEntity(EntityType<? extends ThrowingCardEntity> type, World world)
	{
		super(type, world);
	}

	public ThrowingCardEntity(EntityType<? extends ThrowingCardEntity> type, LivingEntity thrower, World world)
	{
		super(type, thrower, world);
	}

	public ThrowingCardEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(IllagerEntityTypes.THROWING_CARD, world);
	}

	@Override
	protected void onImpact(RayTraceResult result)
	{
		RayTraceResult.Type raytraceresult$type = result.getType();
		if (raytraceresult$type == RayTraceResult.Type.BLOCK)
		{
			BlockRayTraceResult traceResult = (BlockRayTraceResult) result;
			BlockState blockstate = this.world.getBlockState(traceResult.getPos());
			blockstate.onProjectileCollision(this.world, blockstate, traceResult, this);

			this.remove();
		}

		if (raytraceresult$type == RayTraceResult.Type.ENTITY)
		{
			Entity entity = ((EntityRayTraceResult) result).getEntity();

			// func_234616_v_ = getThrower
			if (this.func_234616_v_() != null && !entity.isOnSameTeam(this.func_234616_v_()) && !(entity instanceof RabbitEntity))
			{
				int i = 2 + this.world.getDifficulty().getId();

				if (!entity.isInvulnerable())
				{
					entity.attackEntityFrom(DamageSource.causeThrownDamage(this, this.func_234616_v_()).setDifficultyScaled(), (float) i);
					this.remove();
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.ticksExisted % 8 == 0)
		{
			this.world.addParticle(ParticleTypes.CRIT, this.getPosXRandom(0.5D), this.getPosYRandom(), this.getPosZRandom(0.5D), this.rand.nextGaussian() * 0.1D, 0.0D, this.rand.nextGaussian() * 0.1D);
		}
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("cardType", this.getCardType());
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
		this.setCardType(compound.getInt("cardType"));
	}

	@Override
	protected void registerData()
	{
		this.dataManager.register(CARD_TYPE, Integer.valueOf(0));
	}

	public int getCardType()
	{
		return this.dataManager.get(CARD_TYPE);
	}

	public void setCardType(int type)
	{
		this.dataManager.set(CARD_TYPE, Integer.valueOf(type));
	}
}
