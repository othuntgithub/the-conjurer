package com.legacy.conjurer_illager.client.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class MagicianHatModel<T extends LivingEntity> extends BipedModel<T>
{
	public ModelRenderer hattop;
	public ModelRenderer hatbottom;

	public MagicianHatModel()
	{
		super(1.0F);

		this.textureWidth = 128;
		this.textureHeight = 64;

		float offsetAmount = 2;
		this.hatbottom = new ModelRenderer(this, 84, 16);
		this.hatbottom.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.hatbottom.addBox(-5.5F, -9.0F + offsetAmount, -5.5F, 11, 2, 11, 0.01F); // 0.45
		this.hattop = new ModelRenderer(this, 96, 0);
		this.hattop.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.hattop.addBox(-4.0F, -17.0F + offsetAmount, -4.0F, 8, 8, 8, 0.01F);
	}

	@Override
	protected Iterable<ModelRenderer> getHeadParts()
	{
		float offset = this.bipedHead.rotationPointY;

		this.hattop.copyModelAngles(this.bipedHead);
		this.hatbottom.copyModelAngles(this.bipedHead);

		this.hattop.rotationPointY = offset;
		this.hatbottom.rotationPointY = offset;

		return ImmutableList.of(this.hatbottom, this.hattop);
	}

	@Override
	protected Iterable<ModelRenderer> getBodyParts()
	{
		return ImmutableList.of();
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);

		/*if (!(entityIn instanceof ArmorStandEntity))
		{
			float offset = this.bipedHead.rotationPointY;
		
			this.hattop.copyModelAngles(this.bipedHead);
			this.hatbottom.copyModelAngles(this.bipedHead);
		
			this.hattop.rotationPointY = offset;
			this.hatbottom.rotationPointY = offset;
		}*/

	}

	public void setRotateAngle(ModelRenderer ModelRenderer, float x, float y, float z)
	{
		ModelRenderer.rotateAngleX = x;
		ModelRenderer.rotateAngleY = y;
		ModelRenderer.rotateAngleZ = z;
	}
}
