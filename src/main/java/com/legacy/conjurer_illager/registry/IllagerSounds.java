package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.IllagerRegistry;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class IllagerSounds
{
	public static SoundEvent ENTITY_CONJURER_IDLE, ENTITY_CONJURER_CELEBRATE, ENTITY_CONJURER_HURT, ENTITY_CONJURER_DEATH;
	public static SoundEvent ENTITY_CONJURER_DISAPPEAR, ENTITY_CONJURER_CAST_SPELL, ENTITY_CONJURER_PREPARE_VANISH, ENTITY_CONJURER_PREPARE_RABBIT, ENTITY_CONJURER_PREPARE_ATTACK, ENTITY_CONJURER_PREPARE_DISPLACEMENT;
	
	public static SoundEvent ENTITY_BOUNCY_BALL_BOUNCE, ENTITY_BOUNCY_BALL_VANISH;

	public static IForgeRegistry<SoundEvent> soundRegistry;

	public static void init(Register<SoundEvent> event)
	{
		soundRegistry = event.getRegistry();

		ENTITY_CONJURER_IDLE = register("entity.conjurer.idle");
		ENTITY_CONJURER_CELEBRATE = register("entity.conjurer.celebrate");
		ENTITY_CONJURER_HURT = register("entity.conjurer.hurt");
		ENTITY_CONJURER_DEATH = register("entity.conjurer.death");

		ENTITY_CONJURER_CAST_SPELL = register("entity.conjurer.cast_spell");
		ENTITY_CONJURER_DISAPPEAR = register("entity.conjurer.disappear");
		ENTITY_CONJURER_PREPARE_VANISH = register("entity.conjurer.prepare_vanish");
		ENTITY_CONJURER_PREPARE_RABBIT = register("entity.conjurer.prepare_rabbit");
		ENTITY_CONJURER_PREPARE_ATTACK = register("entity.conjurer.prepare_attack");
		ENTITY_CONJURER_PREPARE_DISPLACEMENT = register("entity.conjurer.prepare_displacement");

		ENTITY_BOUNCY_BALL_BOUNCE = register("entity.bouncy_ball.bounce");
		ENTITY_BOUNCY_BALL_VANISH = register("entity.bouncy_ball.vanish");
	}

	private static SoundEvent register(String name)
	{
		ResourceLocation location = ConjurerIllagerMod.locate(name);

		SoundEvent sound = new SoundEvent(location);

		//sound.setRegistryName(location);

		IllagerRegistry.register(soundRegistry, name, sound);

		return sound;
	}

}