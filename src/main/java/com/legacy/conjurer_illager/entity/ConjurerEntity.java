package com.legacy.conjurer_illager.entity;

import java.util.EnumSet;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerParticles;
import com.legacy.conjurer_illager.registry.IllagerSounds;
import com.legacy.conjurer_illager.registry.IllagerStructures;
import com.legacy.structure_gel.access_helpers.EntityAccessHelper;

import net.minecraft.block.Blocks;
import net.minecraft.entity.CreatureAttribute;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.monster.AbstractIllagerEntity;
import net.minecraft.entity.monster.AbstractRaiderEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.monster.SpellcastingIllagerEntity;
import net.minecraft.entity.monster.VexEntity;
import net.minecraft.entity.passive.IronGolemEntity;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootTables;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.server.ServerWorld;

public class ConjurerEntity extends SpellcastingIllagerEntity
{
	private static final Vector3i BOUNCY_BALL_SPELL_COLOR = new Vector3i(243, 162, 43);
	private static final Vector3i RABBIT_SPELL_COLOR = new Vector3i(255, 208, 208);
	private static final Vector3i DISPLACE_SPELL_COLOR = new Vector3i(165, 96, 201);
	private static final Vector3i DISAPPEAR_SPELL_COLOR = new Vector3i(77, 77, 204);

	public int throwingCardCooldown = 0;

	public ConjurerEntity(EntityType<? extends ConjurerEntity> type, World worldIn)
	{
		super(type, worldIn);
		this.experienceValue = 15;
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(1, new ConjurerEntity.CastingSpellGoal());
		this.goalSelector.addGoal(4, new ConjurerEntity.DisappearSpellGoal());
		this.goalSelector.addGoal(4, new ConjurerEntity.ThrowingCardAttackGoal());
		this.goalSelector.addGoal(5, new ConjurerEntity.BouncyBallSpellGoal());
		this.goalSelector.addGoal(6, new ConjurerEntity.BunnySpellGoal());
		this.goalSelector.addGoal(6, new ConjurerEntity.DisplaceSpellGoal());

		this.goalSelector.addGoal(9, new LookAtGoal(this, PlayerEntity.class, 15.0F, 2.0F));
		this.goalSelector.addGoal(10, new LookAtGoal(this, LivingEntity.class, 8.0F));
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(2, (new NearestAttackableTargetGoal<PlayerEntity>(this, PlayerEntity.class, true)
		{
			@Override
			public boolean shouldExecute()
			{
				List<AbstractRaiderEntity> nearbyRaiders = this.goalOwner.world.getEntitiesWithinAABB(AbstractRaiderEntity.class, new AxisAlignedBB(this.goalOwner.getPosition()).grow(25, 15, 25));
				return super.shouldExecute() && nearbyRaiders.size() < 2;
			}
		}).setUnseenMemoryTicks(300));
		this.targetSelector.addGoal(3, (new NearestAttackableTargetGoal<>(this, AbstractVillagerEntity.class, false)).setUnseenMemoryTicks(300));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, IronGolemEntity.class, false));

		this.goalSelector.addGoal(2, new AvoidEntityGoal<PlayerEntity>(this, PlayerEntity.class, 7.0F, 0.6D, 1.0D)
		{
			@Override
			public boolean shouldExecute()
			{
				List<AbstractRaiderEntity> nearbyRaiders = this.entity.world.getEntitiesWithinAABB(AbstractRaiderEntity.class, new AxisAlignedBB(this.entity.getPosition()).grow(25, 15, 25));
				return super.shouldExecute() && (nearbyRaiders.size() < 2 || this.entity.getAttackTarget() != null);
			}
		});
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MonsterEntity.func_234295_eP_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D).createMutableAttribute(Attributes.FOLLOW_RANGE, 25.0D).createMutableAttribute(Attributes.MAX_HEALTH, 120.0D);
	}

	@Override
	protected void registerData()
	{
		super.registerData();
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		// only allow the conjurer to wander when spawned outside of the theatre
		if (worldIn instanceof ServerWorld)
		{
			BlockPos pos = this.getPosition();
			StructureManager strucManager = ((ServerWorld) worldIn.getWorld()).func_241112_a_();

			boolean inTheatre = strucManager.getStructureStart(pos, false, IllagerStructures.THEATER.getStructure()).isValid();
			if (!inTheatre || reason != SpawnReason.STRUCTURE || (reason == SpawnReason.SPAWN_EGG || reason == SpawnReason.COMMAND) && inTheatre)
			{
				this.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(this, 0.6D));
			}
		}

		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
	}

	@Override
	protected void updateAITasks()
	{
		super.updateAITasks();
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.world.isRemote && this.isSpellcasting())
		{
			float f = this.renderYawOffset * ((float) Math.PI / 180F) + MathHelper.cos((float) this.ticksExisted * 0.6662F) * 0.25F;
			float f1 = MathHelper.cos(f);
			float f2 = MathHelper.sin(f);

			if (this.ticksExisted % 4 == 0)
			{
				double r = this.rand.nextGaussian() * 0.03D;
				double g = 0.08D;
				double b = this.rand.nextGaussian() * 0.03D;

				IParticleData particle = this.rand.nextBoolean() ? IllagerParticles.BLACK_PLAYING_CARD : IllagerParticles.RED_PLAYING_CARD;
				this.world.addParticle(particle, this.getPosX() + (double) f1 * 0.6D, this.getPosY() + 1.8D, this.getPosZ() + (double) f2 * 0.6D, r, g, b);
				this.world.addParticle(particle, this.getPosX() - (double) f1 * 0.6D, this.getPosY() + 1.8D, this.getPosZ() - (double) f2 * 0.6D, r, g, b);
			}

			double d0 = this.getSpellColors().getX() / 255.0D;
			double d1 = this.getSpellColors().getY() / 255.0D;
			double d2 = this.getSpellColors().getZ() / 255.0D;

			this.world.addParticle(ParticleTypes.ENTITY_EFFECT, this.getPosX() + (double) f1 * 0.6D, this.getPosY() + 1.8D, this.getPosZ() + (double) f2 * 0.6D, d0, d1, d2);
			this.world.addParticle(ParticleTypes.ENTITY_EFFECT, this.getPosX() - (double) f1 * 0.6D, this.getPosY() + 1.8D, this.getPosZ() - (double) f2 * 0.6D, d0, d1, d2);
		}

		if (this.throwingCardCooldown > 0)
			--this.throwingCardCooldown;
	}

	private Vector3i getSpellColors()
	{
		SpellType spellType = this.getSpellType();

		// bouncy balls 73
		if (spellType == SpellType.FANGS)
			return BOUNCY_BALL_SPELL_COLOR;
		// rabbit 74
		else if (spellType == SpellType.SUMMON_VEX)
			return RABBIT_SPELL_COLOR;
		// displace 75
		else if (spellType == SpellType.WOLOLO)
			return DISPLACE_SPELL_COLOR;
		// disappear
		else if (spellType == SpellType.DISAPPEAR)
			return DISAPPEAR_SPELL_COLOR;
		else
			return Vector3i.NULL_VECTOR;
	}

	@Override
	public void onDeath(DamageSource cause)
	{
		super.onDeath(cause);

		if (this.world.isRemote)
		{
			for (int i = 0; i < 20; ++i)
			{
				double d0 = this.rand.nextGaussian() * 0.02D;
				double d1 = 0.25D;
				double d2 = this.rand.nextGaussian() * 0.02D;

				IParticleData particle = this.rand.nextBoolean() ? IllagerParticles.BLACK_PLAYING_CARD : IllagerParticles.RED_PLAYING_CARD;
				this.world.addParticle(particle, this.getPosXWidth(1.0D) - d0 * 10.0D, this.getPosYRandom() - d1 * 10.0D + 1, this.getPosZRandom(1.0D) - d2 * 10.0D, d0, d1, d2);
			}
		}
	}

	@Override
	public boolean isOnSameTeam(Entity entityIn)
	{
		if (entityIn == null)
		{
			return false;
		}
		else if (entityIn == this)
		{
			return true;
		}
		else if (super.isOnSameTeam(entityIn))
		{
			return true;
		}
		else if (entityIn instanceof VexEntity)
		{
			return this.isOnSameTeam(((VexEntity) entityIn).getOwner());
		}
		else if (entityIn instanceof LivingEntity && ((LivingEntity) entityIn).getCreatureAttribute() == CreatureAttribute.ILLAGER)
		{
			return this.getTeam() == null && entityIn.getTeam() == null;
		}
		else
		{
			return false;
		}
	}

	@Override
	protected float getSoundPitch()
	{
		return (this.rand.nextFloat() - this.rand.nextFloat()) * 0.1F + 0.9F;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return IllagerSounds.ENTITY_CONJURER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return IllagerSounds.ENTITY_CONJURER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return IllagerSounds.ENTITY_CONJURER_DEATH;
	}

	@Override
	protected SoundEvent getSpellSound()
	{
		return IllagerSounds.ENTITY_CONJURER_CAST_SPELL;
	}

	@Override
	public void applyWaveBonus(int p_213660_1_, boolean p_213660_2_)
	{
	}

	@Override
	public SoundEvent getRaidLossSound()
	{
		return IllagerSounds.ENTITY_CONJURER_CELEBRATE;
	}

	@Override
	public AbstractIllagerEntity.ArmPose getArmPose()
	{
		if (this.getHeldItemMainhand().getItem() == Items.PAPER)
			return ArmPose.CROSSBOW_HOLD;
		else
			return super.getArmPose();
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		return source == DamageSource.OUT_OF_WORLD ? super.attackEntityFrom(source, amount) : source.isCreativePlayer() ? super.attackEntityFrom(source, amount) : super.attackEntityFrom(source, Math.min(15, amount));
	}

	public boolean isEntityLooking(LivingEntity entity)
	{
		if (entity != null)
		{
			Vector3d vec3d = entity.getLook(1.0F).normalize();
			Vector3d vec3d1 = new Vector3d(this.getPosX() - entity.getPosX(), this.getBoundingBox().minY + (double) this.getEyeHeight() - (entity.getPosY() + (double) entity.getEyeHeight()), this.getPosZ() - entity.getPosZ());
			double d0 = vec3d1.length();
			vec3d1 = vec3d1.normalize();
			double d1 = vec3d.dotProduct(vec3d1);

			if (d1 < 1.0D / d0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	class CastingSpellGoal extends SpellcastingIllagerEntity.CastingASpellGoal
	{
		private CastingSpellGoal()
		{
			super();
		}

		@Override
		public void tick()
		{
			if (ConjurerEntity.this.getAttackTarget() != null)
			{
				ConjurerEntity.this.getLookController().setLookPositionWithEntity(ConjurerEntity.this.getAttackTarget(), (float) ConjurerEntity.this.getHorizontalFaceSpeed(), (float) ConjurerEntity.this.getVerticalFaceSpeed());
			}
		}
	}

	class DisappearSpellGoal extends SpellcastingIllagerEntity.UseSpellGoal
	{
		private DisappearSpellGoal()
		{
			super();
		}

		@Override
		public boolean shouldExecute()
		{
			return ConjurerEntity.this.getAttackTarget() != null && (ConjurerEntity.this.hurtTime > 0 || ConjurerEntity.this.getDistance(ConjurerEntity.this.getAttackTarget()) < 8.0F) && !ConjurerEntity.this.isPotionActive(Effects.INVISIBILITY) && ConjurerEntity.this.isEntityLooking(ConjurerEntity.this.getAttackTarget()) && super.shouldExecute();
		}

		// Time the Conjurer is in casting animation
		@Override
		protected int getCastingTime()
		{
			return 30;
		}

		// Time between casts
		@Override
		protected int getCastingInterval()
		{
			return 200;
		}

		@Override
		protected void castSpell()
		{
			ConjurerEntity.this.addPotionEffect(new EffectInstance(Effects.INVISIBILITY, 120 + (ConjurerEntity.this.world.rand.nextInt(3) * 10), 0));
			ConjurerEntity.this.playSound(IllagerSounds.ENTITY_CONJURER_DISAPPEAR, 1.0F, 1.0F);
			ConjurerEntity.this.spawnExplosionParticle();

			if (ConjurerEntity.this.getAttackTarget() == null)
				return;

			if (ConjurerEntity.this.world.rand.nextFloat() <= 0.50F && ConjurerEntity.this.getDistance(ConjurerEntity.this.getAttackTarget()) < 7.0F || ConjurerEntity.this.hurtTime > 0 || ConjurerEntity.this.getDistance(ConjurerEntity.this.getAttackTarget()) <= 4.0F)
			{
				ConjurerEntity.this.setLocationAndAngles(ConjurerEntity.this.getAttackTarget().getPosX(), ConjurerEntity.this.getAttackTarget().getPosY(), ConjurerEntity.this.getAttackTarget().getPosZ(), ConjurerEntity.this.getAttackTarget().getRotationYawHead(), ConjurerEntity.this.getAttackTarget().rotationPitch);
				ConjurerEntity.this.moveRelative(2.0F, new Vector3d(0, 0, -4));
			}
		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return IllagerSounds.ENTITY_CONJURER_PREPARE_VANISH;
		}

		@Override
		protected SpellcastingIllagerEntity.SpellType getSpellType()
		{
			return SpellcastingIllagerEntity.SpellType.DISAPPEAR;
		}
	}

	class BouncyBallSpellGoal extends SpellcastingIllagerEntity.UseSpellGoal
	{
		private BouncyBallSpellGoal()
		{
			super();
		}

		@Override
		public boolean shouldExecute()
		{
			return super.shouldExecute();
		}

		@Override
		protected int getCastingTime()
		{
			return 40;
		}

		@Override
		protected int getCastingInterval()
		{
			return 220;
		}

		@Override
		protected void castSpell()
		{
			double x = ConjurerEntity.this.getAttackTarget().getPosX() - ConjurerEntity.this.getPosX();
			double y = (ConjurerEntity.this.getAttackTarget().getBoundingBox().minY + (ConjurerEntity.this.getAttackTarget().getHeight() / 2.0F)) - (ConjurerEntity.this.getPosY() + (ConjurerEntity.this.getHeight() / 2.0F));
			double z = ConjurerEntity.this.getAttackTarget().getPosZ() - ConjurerEntity.this.getPosZ();

			Vector3d lookVector = ConjurerEntity.this.getLook(1.0F);

			for (int i = 0; i < 5; ++i)
			{
				BouncingBallEntity projectile = new BouncingBallEntity(IllagerEntityTypes.BOUNCING_BALL, ConjurerEntity.this, ConjurerEntity.this.world);

				Random rand = ConjurerEntity.this.world.rand;
				projectile.setNoGravity(!ConjurerEntity.this.world.canSeeSky(ConjurerEntity.this.getPosition()));
				projectile.shoot(x + (i * 5) - 10, y + rand.nextInt(3), z, 0.5F, 1.0F);
				projectile.setPosition(ConjurerEntity.this.getPosX() + lookVector.x * 1.0D, ConjurerEntity.this.getPosY() + (double) (ConjurerEntity.this.getHeight() / 2.0F) + 0.2F, ConjurerEntity.this.getPosZ() + lookVector.z * 1.0D);
				projectile.setBallType(rand.nextInt(5));

				if (!ConjurerEntity.this.world.isRemote)
				{
					ConjurerEntity.this.world.addEntity(projectile);
				}
			}
		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return IllagerSounds.ENTITY_CONJURER_PREPARE_ATTACK;
		}

		@Override
		protected SpellcastingIllagerEntity.SpellType getSpellType()
		{
			return SpellType.FANGS;
		}
	}

	class BunnySpellGoal extends SpellcastingIllagerEntity.UseSpellGoal
	{

		private BunnySpellGoal()
		{
			super();
		}

		@Override
		public boolean shouldExecute()
		{
			List<RabbitEntity> nearbyRabbits = ConjurerEntity.this.world.getEntitiesWithinAABB(RabbitEntity.class, new AxisAlignedBB(ConjurerEntity.this.getPosition()).grow(25, 15, 25));

			if (!super.shouldExecute() || ConjurerEntity.this.getAttackTarget() == null || ConjurerEntity.this.isPotionActive(Effects.INVISIBILITY))
			{
				return false;
			}
			else
			{
				return super.shouldExecute() && nearbyRabbits.size() < 3;
			}
		}

		@Override
		protected int getCastingTime()
		{
			return 50;
		}

		@Override
		protected int getCastingInterval()
		{
			return 300;
		}

		@Override
		protected void castSpell()
		{
			Vector3d lookVector = ConjurerEntity.this.getLook(1.0F);
			double x = ConjurerEntity.this.getPosX() + lookVector.x;
			double y = ConjurerEntity.this.getPosY() + (double) (ConjurerEntity.this.getHeight() / 2.0F + 2.0D);
			double z = ConjurerEntity.this.getPosZ() + lookVector.z;
			BlockPos blockpos = (new BlockPos(x, y, z));
			RabbitEntity rabbit = EntityType.RABBIT.create(ConjurerEntity.this.world);

			rabbit.goalSelector.addGoal(1, new MeleeAttackGoal(rabbit, 1.4D, true));
			rabbit.targetSelector.addGoal(1, new HurtByTargetGoal(rabbit));
			rabbit.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(rabbit, PlayerEntity.class, true));
			rabbit.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(rabbit, VillagerEntity.class, true));

			rabbit.getAttribute(Attributes.ARMOR).setBaseValue(8.0D);
			rabbit.getAttribute(Attributes.MAX_HEALTH).setBaseValue(10.0D);
			rabbit.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(0.45D);

			rabbit.setAttackTarget(ConjurerEntity.this.getAttackTarget());
			EntityAccessHelper.setDeathLootTable(rabbit, LootTables.EMPTY);

			rabbit.setRabbitType(rand.nextBoolean() ? 3 : 1);
			rabbit.setMotion(rabbit.getMotion().getX(), 0.3F, rabbit.getMotion().getZ());
			rabbit.moveToBlockPosAndAngles(blockpos, ConjurerEntity.this.rotationYawHead, ConjurerEntity.this.rotationPitch);
			rabbit.moveRelative(0.3F, new Vector3d(0, 0, 1));
			rabbit.setHealth(10);

			if (!rabbit.isOnSameTeam(ConjurerEntity.this) && ConjurerEntity.this.getTeam() != null)
				ConjurerEntity.this.world.getScoreboard().addPlayerToTeam(rabbit.getUniqueID().toString(), ConjurerEntity.this.world.getScoreboard().getTeam(ConjurerEntity.this.getTeam().getName()));

			ConjurerEntity.this.world.addEntity(rabbit);
		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return IllagerSounds.ENTITY_CONJURER_PREPARE_RABBIT;
		}

		@Override
		protected SpellcastingIllagerEntity.SpellType getSpellType()
		{
			return SpellType.SUMMON_VEX;
		}
	}

	class DisplaceSpellGoal extends SpellcastingIllagerEntity.UseSpellGoal
	{
		private DisplaceSpellGoal()
		{
			super();
		}

		@Override
		public boolean shouldExecute()
		{
			if (ConjurerEntity.this.getAttackTarget() != null && ConjurerEntity.this.isPotionActive(Effects.INVISIBILITY))
			{
				return super.shouldExecute();
			}
			else
			{
				return false;
			}

		}

		// Time the Conjurer is in casting animation
		@Override
		protected int getCastingTime()
		{
			return 60;
		}

		// Time between casts
		@Override
		protected int getCastingInterval()
		{
			return 460;
		}

		@Override
		protected void castSpell()
		{
			LivingEntity entityLiving = ConjurerEntity.this.getAttackTarget();
			this.teleportEntity(entityLiving, true);
			// this.teleportEntity(conjurer, true);
		}

		public void teleportEntity(LivingEntity entityLiving, boolean strictIn)
		{
			World worldIn = entityLiving.world;
			double d0 = entityLiving.getPosX();
			double d1 = entityLiving.getPosY();
			double d2 = entityLiving.getPosZ();

			for (int i = 0; i < 30; ++i)
			{
				double d3 = entityLiving.getPosX() + (entityLiving.getRNG().nextDouble() - 0.5D) * 16.0D;
				double d4 = MathHelper.clamp(entityLiving.getPosY() + (double) (entityLiving.getRNG().nextInt(5) - 2), 0.0D, (double) (worldIn.getHeight() - 1));
				double d5 = entityLiving.getPosZ() + (entityLiving.getRNG().nextDouble() - 0.5D) * 16.0D;

				if (entityLiving.isPassenger())
				{
					entityLiving.stopRiding();
				}

				boolean isProperBlock;

				if (!ConjurerEntity.this.world.canSeeSky(ConjurerEntity.this.getPosition()))
				{
					if (strictIn)
					{
						BlockPos pos = new BlockPos(d3, d4, d5);
						isProperBlock = worldIn.getBlockState(pos.down()).getBlock() == Blocks.BIRCH_PLANKS || worldIn.getBlockState(pos.down()).getBlock() == Blocks.STRIPPED_DARK_OAK_LOG || worldIn.getBlockState(pos.down()).getBlock() == Blocks.STRIPPED_DARK_OAK_WOOD || worldIn.getBlockState(pos.down()).getBlock() == Blocks.BIRCH_STAIRS;
					}
					else
						isProperBlock = !worldIn.canBlockSeeSky(new BlockPos(d3, d4, d5));
				}
				else
					isProperBlock = true;

				if (isProperBlock && entityLiving.attemptTeleport(d3, d4, d5, true))
				{
					worldIn.playSound((PlayerEntity) null, d0, d1, d2, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1.0F, 1.0F);
					entityLiving.playSound(SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, 1.0F, 1.0F);
					break;
				}
			}

		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return IllagerSounds.ENTITY_CONJURER_PREPARE_DISPLACEMENT;
		}

		@Override
		protected SpellcastingIllagerEntity.SpellType getSpellType()
		{
			return SpellType.WOLOLO;
		}
	}

	class ThrowingCardAttackGoal extends Goal
	{
		public int cardThrows = 0;

		private ThrowingCardAttackGoal()
		{
			super();
			this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
		}

		@Override
		public boolean shouldExecute()
		{
			return ConjurerEntity.this.throwingCardCooldown <= 0 && this.cardThrows < 10 && !ConjurerEntity.this.isSpellcasting() && ConjurerEntity.this.getAttackTarget() != null && ConjurerEntity.this.getAttackTarget().world.rand.nextInt(20) == 0/* && ConjurerEntity.this.getDistance(ConjurerEntity.this.getAttackTarget()) > 10.0F*/;
		}

		@Override
		public void resetTask()
		{
			super.resetTask();

			this.cardThrows = 0;

			ConjurerEntity.this.setItemStackToSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
		}

		@Override
		public boolean shouldContinueExecuting()
		{
			return ConjurerEntity.this.getAttackTarget() != null && !ConjurerEntity.this.isSpellcasting() && this.cardThrows < 10;
		}

		@Override
		public void startExecuting()
		{
			super.startExecuting();

			ConjurerEntity.this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.PAPER));
		}

		@Override
		public void tick()
		{
			super.tick();

			ConjurerEntity conjurer = ConjurerEntity.this;

			if (conjurer.ticksExisted % 4 == 0 && cardThrows < 10)
			{
				conjurer.swingArm(Hand.MAIN_HAND);
				++cardThrows;
				double x = ConjurerEntity.this.getAttackTarget().getPosX() - ConjurerEntity.this.getPosX();
				double y = (ConjurerEntity.this.getAttackTarget().getBoundingBox().minY + (ConjurerEntity.this.getAttackTarget().getHeight() / 2.0F)) - (ConjurerEntity.this.getPosY() + (ConjurerEntity.this.getHeight() / 2.0F));
				double z = ConjurerEntity.this.getAttackTarget().getPosZ() - ConjurerEntity.this.getPosZ();

				Vector3d lookVector = ConjurerEntity.this.getLook(1.0F);

				ThrowingCardEntity projectile = new ThrowingCardEntity(IllagerEntityTypes.THROWING_CARD, ConjurerEntity.this, ConjurerEntity.this.world);

				Random rand = ConjurerEntity.this.world.rand;
				projectile.shoot(x, y, z, 1.2F, 0.5F);
				projectile.setPosition(ConjurerEntity.this.getPosX() + lookVector.x * 1.0D, ConjurerEntity.this.getPosY() + (double) (ConjurerEntity.this.getHeight() / 2.0F) + 0.2F, ConjurerEntity.this.getPosZ() + lookVector.z * 1.0D);

				conjurer.playSound(SoundEvents.ITEM_BOOK_PAGE_TURN, 1.0F, 1.0F);

				projectile.setCardType((rand.nextFloat() < 0.1F ? 2 : rand.nextInt(2)));
				if (!ConjurerEntity.this.world.isRemote)
				{
					ConjurerEntity.this.world.addEntity(projectile);
				}

				if (cardThrows == 9)
				{
					ConjurerEntity.this.setItemStackToSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
					conjurer.throwingCardCooldown = 200;
				}
			}

			if (ConjurerEntity.this.getAttackTarget() != null)
			{
				ConjurerEntity.this.getLookController().setLookPositionWithEntity(ConjurerEntity.this.getAttackTarget(), (float) ConjurerEntity.this.getHorizontalFaceSpeed(), (float) ConjurerEntity.this.getVerticalFaceSpeed());
			}
		}

		@Override
		public boolean isPreemptible()
		{
			return false;
		}
	}
}