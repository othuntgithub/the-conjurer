package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.IllagerRegistry;
import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.entity.ThrowingCardEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(ConjurerIllagerMod.MODID)
public class IllagerEntityTypes
{
	public static final EntityType<ConjurerEntity> CONJURER = buildEntity("conjurer", EntityType.Builder.create(ConjurerEntity::new, EntityClassification.MONSTER).size(0.6F, 1.95F));
	public static final EntityType<BouncingBallEntity> BOUNCING_BALL = buildEntity("bouncing_ball", EntityType.Builder.<BouncingBallEntity>create(BouncingBallEntity::new, EntityClassification.MISC).setCustomClientFactory(BouncingBallEntity::new).immuneToFire().setShouldReceiveVelocityUpdates(true).size(0.4F, 0.4F));
	public static final EntityType<ThrowingCardEntity> THROWING_CARD = buildEntity("throwing_card", EntityType.Builder.<ThrowingCardEntity>create(ThrowingCardEntity::new, EntityClassification.MISC).setCustomClientFactory(ThrowingCardEntity::new).immuneToFire().setShouldReceiveVelocityUpdates(true).size(0.3F, 0.1F));

	public static void init(Register<EntityType<?>> event)
	{
		IllagerRegistry.register(event.getRegistry(), "conjurer", CONJURER);
		IllagerRegistry.register(event.getRegistry(), "bouncing_ball", BOUNCING_BALL);
		IllagerRegistry.register(event.getRegistry(), "throwing_card", THROWING_CARD);

		registerSpawnConditions();
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(ConjurerIllagerMod.find(key));
	}

	private static void registerSpawnConditions()
	{
		EntitySpawnPlacementRegistry.register(IllagerEntityTypes.CONJURER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, ConjurerEntity::canSpawnOn);
	}
}
