package com.legacy.conjurer_illager.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.conjurer_illager.entity.ConjurerEntity;

import net.minecraft.entity.monster.SpellcastingIllagerEntity;

@Mixin(SpellcastingIllagerEntity.class)
public class SpellcastingIllagerEntityMixin
{
	@Inject(at = @At(value = "INVOKE_ASSIGN", target = "net/minecraft/entity/monster/SpellcastingIllagerEntity.getSpellType()Lnet/minecraft/entity/monster/SpellcastingIllagerEntity$SpellType;"), method = "tick()V", cancellable = true)
	private void tick(CallbackInfo callback)
	{
		if (((SpellcastingIllagerEntity) (Object) this) instanceof ConjurerEntity)
			callback.cancel();
	}
}
