package com.legacy.conjurer_illager.client;

import com.legacy.conjurer_illager.IllagerRegistry;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.IDyeableArmorItem;

public class IllagerColoring
{
	public static class ItemCol extends ItemColors
	{
		public static void init()
		{
			ItemColors itemColors = Minecraft.getInstance().getItemColors();
			itemColors.register((stack, layer) ->
			{
				return layer == 0 ? ((IDyeableArmorItem) stack.getItem()).getColor(stack) : -1;
			}, IllagerRegistry.CONJURER_HAT);
		}
	}
}
