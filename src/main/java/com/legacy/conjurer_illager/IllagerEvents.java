package com.legacy.conjurer_illager;

import java.util.Locale;
import java.util.Random;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.registry.IllagerStructures;

import net.minecraft.entity.Entity;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.FilledMapItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffer;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.MapData;
import net.minecraft.world.storage.MapDecoration;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.PotionEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class IllagerEvents
{
	@SubscribeEvent
	public void onPotionEffectGiven(PotionEvent.PotionApplicableEvent event)
	{
		if (event.getPotionEffect().getPotion().getEffect() == Effects.BAD_OMEN && event.getEntityLiving().getItemStackFromSlot(EquipmentSlotType.HEAD).getItem() == IllagerRegistry.CONJURER_HAT)
			event.setResult(Result.DENY);
	}

	@SubscribeEvent
	public void onLivingAttack(LivingAttackEvent event)
	{
		if (event.getAmount() > 0 && event.getSource().getImmediateSource() instanceof RabbitEntity)
			event.getSource().getImmediateSource().playSound(SoundEvents.ENTITY_RABBIT_ATTACK, 1.0F, (event.getEntityLiving().world.rand.nextFloat() - event.getEntityLiving().world.rand.nextFloat()) * 0.2F + 1.0F);
	}

	@SubscribeEvent
	public void onVillagerTradesAssigned(VillagerTradesEvent event)
	{
		try
		{
			if (event.getType() == VillagerProfession.CARTOGRAPHER)
				event.getTrades().get(2).add(new IllagerEvents.EmeraldForMapTrade(11, IllagerStructures.THEATER.getStructure(), MapDecoration.Type.RED_X, 15, 5));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	static class EmeraldForMapTrade implements VillagerTrades.ITrade
	{
		private final int count;
		private final Structure<?> structureName;
		private final MapDecoration.Type mapDecorationType;
		private final int maxUses;
		private final int xpValue;

		public EmeraldForMapTrade(int emeraldCountIn, Structure<?> structureNameIn, MapDecoration.Type mapTypeIn, int maxUsesIn, int experienceValueIn)
		{
			this.count = emeraldCountIn;
			this.structureName = structureNameIn;
			this.mapDecorationType = mapTypeIn;
			this.maxUses = maxUsesIn;
			this.xpValue = experienceValueIn;
		}

		@Nullable
		public MerchantOffer getOffer(Entity trader, Random rand)
		{
			if (!(trader.world instanceof ServerWorld))
			{
				return null;
			}
			else
			{
				ServerWorld serverworld = (ServerWorld) trader.world;
				BlockPos blockpos = serverworld.func_241117_a_(this.structureName, trader.getPosition(), 100, true);
				if (blockpos != null)
				{
					ItemStack itemstack = FilledMapItem.setupNewMap(serverworld, blockpos.getX(), blockpos.getZ(), (byte) 2, true, true);
					FilledMapItem.func_226642_a_(serverworld, itemstack);
					MapData.addTargetDecoration(itemstack, blockpos, "+", this.mapDecorationType);
					itemstack.setDisplayName(new TranslationTextComponent("filled_map." + this.structureName.getStructureName().toLowerCase(Locale.ROOT)));
					return new MerchantOffer(new ItemStack(Items.EMERALD, this.count), new ItemStack(Items.COMPASS), itemstack, this.maxUses, this.xpValue, 0.2F);
				}
				else
				{
					return null;
				}
			}
		}
	}
}