package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.ConjurerIllagerMod;
import com.legacy.conjurer_illager.client.model.MagicianIllagerModel;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.IllagerRenderer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.client.renderer.entity.model.IllagerModel;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ConjurerRenderer<T extends ConjurerEntity> extends IllagerRenderer<T>
{
	private static final ResourceLocation CONJURER_ILLAGER = ConjurerIllagerMod.locate("textures/entity/conjurer_illager.png");

	public ConjurerRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, (IllagerModel<T>) new MagicianIllagerModel<T>(0.0F, 0.0F, 128, 64), 0.5F);
		this.addLayer(new ConjurerSunglassesLayer<>(this));
		this.addLayer(new HeldItemLayer<T, IllagerModel<T>>(this)
		{
			public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				if (entitylivingbaseIn.isSpellcasting())
				{
					super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
				}

			}
		});
	}

	public ResourceLocation getEntityTexture(T entity)
	{
		return CONJURER_ILLAGER;
	}
}