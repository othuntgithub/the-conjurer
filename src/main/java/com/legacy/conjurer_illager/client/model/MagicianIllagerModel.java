package com.legacy.conjurer_illager.client.model;

import com.google.common.collect.ImmutableList;
import com.legacy.conjurer_illager.entity.ConjurerEntity;

import net.minecraft.client.renderer.entity.model.IllagerModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.monster.AbstractIllagerEntity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class MagicianIllagerModel<T extends ConjurerEntity> extends IllagerModel<T>
{
	public ModelRenderer hattop;
	public ModelRenderer hatbottom;

	public ModelRenderer armhatbottom;
	public ModelRenderer armhattop;

	public ModelRenderer bipedCape;

	public ModelRenderer bowtie;

	public ModelRenderer head;
	public ModelRenderer hat;
	public ModelRenderer body;
	public ModelRenderer arms;
	public ModelRenderer rightLeg;
	public ModelRenderer leftLeg;
	public ModelRenderer rightArm;
	public ModelRenderer leftArm;

	public MagicianIllagerModel(float scaleFactor, float p_i47227_2_, int textureWidthIn, int textureHeightIn)
	{
		super(scaleFactor, p_i47227_2_, textureWidthIn, textureHeightIn);

		this.textureWidth = 128;
		this.textureHeight = 64;

		this.hatbottom = new ModelRenderer(this, 84, 16);
		this.hatbottom.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.hatbottom.addBox(-5.5F, -9.0F, -5.5F, 11, 2, 11, 0.01F);
		this.hattop = new ModelRenderer(this, 96, 0);
		this.hattop.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.hattop.addBox(-4.0F, -17.0F, -4.0F, 8, 8, 8, 0.01F);

		this.armhatbottom = new ModelRenderer(this, 102, 42);
		this.armhatbottom.setRotationPoint(-5.0F, 2.0F, 0.0F);
		this.armhatbottom.addBox(-2.5F, 8.5F, -5.5F, 2, 11, 11, 0.0F);

		this.armhattop = new ModelRenderer(this, 70, 48);
		this.armhattop.setRotationPoint(-5.0F, 2.0F, 0.0F);
		this.armhattop.addBox(-0.5F, 10.0F, -4.0F, 8, 8, 8, 0.0F);

		this.head = (new ModelRenderer(this)).setTextureSize(textureWidthIn, textureHeightIn);
		this.head.setRotationPoint(0.0F, 0.0F + p_i47227_2_, 0.0F);
		this.head.setTextureOffset(0, 0).addBox(-4.0F, -10.0F, -4.0F, 8.0F, 10.0F, 8.0F, scaleFactor);
		this.hat = (new ModelRenderer(this, 32, 0)).setTextureSize(textureWidthIn, textureHeightIn);
		this.hat.addBox(-4.0F, -10.0F, -4.0F, 8.0F, 12.0F, 8.0F, scaleFactor + 0.45F);
		this.head.addChild(this.hat);
		this.hat.showModel = false;
		ModelRenderer modelrenderer = (new ModelRenderer(this)).setTextureSize(textureWidthIn, textureHeightIn);
		modelrenderer.setRotationPoint(0.0F, p_i47227_2_ - 2.0F, 0.0F);
		modelrenderer.setTextureOffset(24, 0).addBox(-1.0F, -1.0F, -6.0F, 2.0F, 4.0F, 2.0F, scaleFactor);
		this.head.addChild(modelrenderer);
		this.body = (new ModelRenderer(this)).setTextureSize(textureWidthIn, textureHeightIn);
		this.body.setRotationPoint(0.0F, 0.0F + p_i47227_2_, 0.0F);
		this.body.setTextureOffset(16, 20).addBox(-4.0F, 0.0F, -3.0F, 8.0F, 12.0F, 6.0F, scaleFactor);
		this.body.setTextureOffset(0, 38).addBox(-4.0F, 0.0F, -3.0F, 8.0F, 18.0F, 6.0F, scaleFactor + 0.5F);
		this.arms = (new ModelRenderer(this)).setTextureSize(textureWidthIn, textureHeightIn);
		this.arms.setRotationPoint(0.0F, 0.0F + p_i47227_2_ + 2.0F, 0.0F);
		this.arms.setTextureOffset(44, 22).addBox(-8.0F, -2.0F, -2.0F, 4.0F, 8.0F, 4.0F, scaleFactor);
		ModelRenderer modelrenderer1 = (new ModelRenderer(this, 44, 22)).setTextureSize(textureWidthIn, textureHeightIn);
		modelrenderer1.mirror = true;
		modelrenderer1.addBox(4.0F, -2.0F, -2.0F, 4.0F, 8.0F, 4.0F, scaleFactor);
		this.arms.addChild(modelrenderer1);
		this.arms.setTextureOffset(40, 38).addBox(-4.0F, 2.0F, -2.0F, 8.0F, 4.0F, 4.0F, scaleFactor);
		this.rightLeg = (new ModelRenderer(this, 0, 22)).setTextureSize(textureWidthIn, textureHeightIn);
		this.rightLeg.setRotationPoint(-2.0F, 12.0F + p_i47227_2_, 0.0F);
		this.rightLeg.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, scaleFactor);
		this.leftLeg = (new ModelRenderer(this, 0, 22)).setTextureSize(textureWidthIn, textureHeightIn);
		this.leftLeg.mirror = true;
		this.leftLeg.setRotationPoint(2.0F, 12.0F + p_i47227_2_, 0.0F);
		this.leftLeg.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, scaleFactor);
		this.rightArm = (new ModelRenderer(this, 40, 46)).setTextureSize(textureWidthIn, textureHeightIn);
		this.rightArm.addBox(-3.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, scaleFactor);
		this.rightArm.setRotationPoint(-5.0F, 2.0F + p_i47227_2_, 0.0F);
		this.leftArm = (new ModelRenderer(this, 40, 46)).setTextureSize(textureWidthIn, textureHeightIn);
		this.leftArm.mirror = true;
		this.leftArm.addBox(-1.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, scaleFactor);
		this.leftArm.setRotationPoint(5.0F, 2.0F + p_i47227_2_, 0.0F);

		this.bowtie = new ModelRenderer(this, 32, 0);
		this.bowtie.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bowtie.addBox(-3.5F, 0.0F, -3.3F, 7, 4, 0, 0.0F);

		this.bipedCape = new ModelRenderer(this, 72, 0);
		this.bipedCape.addBox(-5.0F, 0.0F, -1.0F, 10.0F, 16.0F, 1.0F, scaleFactor);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.hatbottom, this.hattop, this.armhatbottom, this.armhattop, this.head, this.rightArm, this.leftArm, this.arms, this.body, this.leftLeg, this.rightLeg, this.bowtie, this.bipedCape);
	}

	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);

		boolean spellcasting = entityIn.getArmPose() == AbstractIllagerEntity.ArmPose.SPELLCASTING;
		boolean celebrating = entityIn.getArmPose() == AbstractIllagerEntity.ArmPose.CELEBRATING;

		ModelRenderer head = ObfuscationReflectionHelper.getPrivateValue(IllagerModel.class, this, "field_191217_a");
		ModelRenderer rightArm = ObfuscationReflectionHelper.getPrivateValue(IllagerModel.class, this, "field_191223_g");
		ModelRenderer leftArm = ObfuscationReflectionHelper.getPrivateValue(IllagerModel.class, this, "field_191224_h");
		ModelRenderer rightLeg = ObfuscationReflectionHelper.getPrivateValue(IllagerModel.class, this, "field_217143_g");
		ModelRenderer leftLeg = ObfuscationReflectionHelper.getPrivateValue(IllagerModel.class, this, "field_217144_h");
		ModelRenderer arms = ObfuscationReflectionHelper.getPrivateValue(IllagerModel.class, this, "field_191219_c");

		this.armhatbottom.showModel = spellcasting || celebrating;
		this.armhattop.showModel = spellcasting || celebrating;
		this.hatbottom.showModel = !spellcasting && !celebrating;
		this.hattop.showModel = !spellcasting && !celebrating;

		this.hatbottom.copyModelAngles(head);
		this.hattop.copyModelAngles(head);
		this.head.copyModelAngles(head);

		this.rightArm.copyModelAngles(rightArm);
		this.leftArm.copyModelAngles(leftArm);

		this.leftLeg.copyModelAngles(leftLeg);
		this.rightLeg.copyModelAngles(rightLeg);

		this.arms.copyModelAngles(arms);
		this.bowtie.copyModelAngles(this.body);

		this.arms.showModel = arms.showModel;
		this.rightArm.showModel = rightArm.showModel;
		this.leftArm.showModel = leftArm.showModel;

		this.bipedCape.rotateAngleX = 0.1F + limbSwingAmount * 0.6F + (celebrating ? 0.5F : 0.0F);
		this.bipedCape.rotationPointZ = 4.0F + (celebrating ? -7.0F : 0.0F);
		this.bipedCape.rotationPointY = -0.6F + (celebrating ? -2.0F : 0.0F);

		if (celebrating)
		{
			this.body.rotateAngleX = 0.5F;
			this.body.rotationPointZ = -6.5F;

			head.rotationPointZ = -7.5F;
			this.head.rotateAngleX = 0.7F + MathHelper.cos(ageInTicks * 0.2F) * 0.1F;

			this.rightArm.rotationPointZ = -5.5F;
			this.rightArm.rotationPointX = -5.0F;
			this.rightArm.rotateAngleX = 0;
			this.rightArm.rotateAngleZ = 2.0F + MathHelper.cos(ageInTicks * 0.1F) * 0.2F;
			this.rightArm.rotateAngleY = 0.0F;

			this.leftArm.rotationPointZ = -5.5F;
			this.leftArm.rotationPointX = 5.0F;

			// forward back
			this.leftArm.rotateAngleX = 0.6F;
			// left right
			this.leftArm.rotateAngleZ = 0.0F;

			this.leftArm.rotateAngleY = 0.0F;

			this.armhatbottom.copyModelAngles(this.rightArm);
			this.armhattop.copyModelAngles(this.rightArm);
		}
		else
		{
			this.armhatbottom.copyModelAngles(rightArm);
			this.armhattop.copyModelAngles(rightArm);

			this.body.rotateAngleX = 0.0F;
			this.body.rotationPointZ = 0.0F;

			head.rotationPointZ = 0.0F;
		}

		if (entityIn.getArmPose() == AbstractIllagerEntity.ArmPose.CROSSBOW_HOLD)
			this.leftArm.rotateAngleY = MathHelper.cos(ageInTicks * 2.0F) * 0.2F;
	}
}
